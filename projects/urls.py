from django.urls import path
from projects.views import views


urlpatterns = [
    path("", views.projects, name="projects"),
]
