from django.urls import path
from accounts.views import views

urlpatterns = [
    path("", views.accounts, name="accounts"),
]
