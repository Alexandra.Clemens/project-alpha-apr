from unittest.util import _MAX_LENGTH
from django.db import models

# Create your models here.


class Tasks(models.Model):
    name = models.CharField(max_length=100)
    related_name = "tasks"

    def __str__(self):
        return self.name
